import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'dart:async';

class LoginPage extends StatefulWidget {
  // This widget is the login screen of the application
  @override
  _LoginPageState createState() => _LoginPageState();
}

enum FormMode{
  LOGIN,
  SIGNUP,
}

class _LoginPageState extends State<LoginPage> {

  final _formKey = GlobalKey<FormState>();

  FirebaseAuth _auth = FirebaseAuth.instance;

  String _email = "";
  String _password = "";
  String _errorMessage = "";

  bool _isLoading;

  FormMode _formMode;

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            _showBody(),
            _showCircularLoadingIndicator(),
          ],
        ),
      )
    );
  }

  @override
  void initState(){
    _formMode = FormMode.LOGIN;
    _isLoading = false;
  }

  Widget _showCircularLoadingIndicator(){
    if(_isLoading){
      return Center(child: CircularProgressIndicator(),);
    }
    return Container(height: 0.0,width: 0.0,);
  }

  // Container with a Form
  Widget _showBody(){
    return Container(
      padding: EdgeInsets.all(16.0),
      child: Form( 
        key: _formKey,
        child: ListView(
          children: <Widget>[
            SizedBox(height: 100,),
            _logo(),
            SizedBox(height: 100,),
            _emailField(),
            SizedBox(height: 10,),
            _passwordField(),
            SizedBox(height: 10,),
            _primaryInputButton(),
            _secondaryInputButton(),
            _errorField(),
          ],
        ),
      ),
    );
  }

  Widget _logo() {
    return Image.asset(
        "assets/images/weddingsLogoNoName.png",
        height: 80.0,
        width: 80.0,
    );
  }

  Widget _emailField(){
    return TextFormField(
      autofocus: false,
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        hintText: "Email",
        icon: Icon(
          Icons.email,
          color: Colors.pink,
        ),
      ),
      validator: (value) {
        return value.isEmpty ?
          "Field can\'t be empty" : null;
      },
      onSaved: (value) {
        _email = value;
      },
    );
  }

  Widget _passwordField() {
    return TextFormField(
      autofocus: false,
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      obscureText: true,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        hintText: "Password",
        icon: Icon(
          Icons.security,
          color: Colors.pink,
        ),
      ),
      validator: (value) {
        return value.isEmpty ?
          "Field can\'t be empty" : null;
      },
      onSaved: (value) {
        _password = value;
      },
    );
  }
  
  Widget _primaryInputButton(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: RaisedButton(
        shape: StadiumBorder(),
        splashColor: Colors.white,
        color: Colors.pink,
        child: _formMode == FormMode.LOGIN ?
          Text("Login") : Text("SignUp"),
        onPressed: _validateAndSubmit,
      ),
    );
  }

  Widget _secondaryInputButton(){
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: FlatButton(
        shape: StadiumBorder(),
        splashColor: Colors.pink,
        child: _formMode == FormMode.LOGIN ?
          Text("Create Account?") : Text("Back to Sign in."),
        onPressed: _formMode == FormMode.LOGIN ?
          _changeToSignUp : _changeToLogin,
      ),
    );
  }

  Widget _errorField(){
    if( !_errorMessage.isEmpty ) {
      return Text(_errorMessage);
    }
    return Container(height: 0.0, width: 0.0,);
  }

  bool _validateAndSave(){
    FormState state = _formKey.currentState;
    if(state.validate()){
      state.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async{
    if(_validateAndSave()){
      String uid = "";

      setState(() {
        _errorMessage = "";
        _isLoading = true;
      });
      try{
        if(_formMode == FormMode.LOGIN) {
          uid = await _login(_email, _password);
          print('UserID: $uid');
        } else {
          uid = await _signUp(_email, _password);
          print('UserID: $uid');
        }
        if(!uid.isEmpty) {
          print("Logged in");
          Navigator.pushNamed(context, '/');
          _isLoading = false;
        }
      } catch (e) {
        setState(() {
          _isLoading = false;
          //TODO: Change Error messages to be User Friendly
          //TODO: with multiple catches
          _errorMessage = e.toString();
        });
        print(e.toString());
      }
    }
  }

  void _changeToSignUp(){
    _formKey.currentState.reset();
    setState(() {
      _isLoading = false;
      _errorMessage = "";
      _formMode = FormMode.SIGNUP;
    });
  }

  void _changeToLogin(){
    _formKey.currentState.reset();
    setState(() {
      _isLoading = false;
      _errorMessage = "";
      _formMode = FormMode.LOGIN;
    });
  }

  Future<String> _signUp(String email, String password) async{
    FirebaseUser user = await _auth.createUserWithEmailAndPassword(
      email: email,
      password: password
    );

    Map<String,String> data = {
      'firstName' : '',
      'lastName' : '',
    };
    
    Firestore.instance.collection('users3').document(user.uid).setData(data);
    
    return user.uid;
  }

  Future<String> _login(String email, String password) async{
    FirebaseUser user = await _auth.signInWithEmailAndPassword(
      email: email,
      password: password
    );
    return user.uid;
  }


}
