import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class AccountPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AccountPageState();
  }
}

class AccountPageState extends State<AccountPage> {
  TextEditingController username = TextEditingController();

  TextEditingController _firstName = TextEditingController();
  TextEditingController _lastName = TextEditingController();
  FirebaseUser _user;

  Future<DocumentSnapshot> snapshot;


  // image
  File _image;

  @override
  void initState() {
    super.initState();
    _setUserFields();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: //_getUsernameAppBar(context),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text("Profile"),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return ListView(

      children: <Widget>[
        _profileTop(),
        _displayOrInputField('First Name', 'firstname', _firstName),
        _displayOrInputField('Last Name', 'lastname', _lastName),
        _pickImage(),
        _venueOption(Colors.greenAccent),
        _venueOption(Colors.blue),
      ],
    );
  }

  Widget _pickImage() {
    return ListTile(
      trailing: IconButton(
          icon: Icon(Icons.image),
          onPressed: getImage
      ),
      title: Container(
        child: _image == null ?
        Text("no Image") : Image.file(_image),
      ),
    );

  }


  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery /*ImageSource.camera*/);

    setState(() {
      _image = image;
    });
  }

  Widget _venueOption(Color color) {
    return Container(
      color: color,
      padding: EdgeInsets.all(8.0),
      height: 200,
      child: ListView.separated(
        padding: EdgeInsets.all(8.0),
        scrollDirection: Axis.horizontal,
        itemCount: 20,
        separatorBuilder: (BuildContext context, int index) => Divider(),
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 200,
            width: 100,
            color: Colors.amberAccent,
            child: Text("Item $index"),
          );
        },
      ),
    );
  }

  Widget _displayOrInputField(String fieldHint,
      String key,
      TextEditingController controller) {
    return ListTile(
      title: TextField(
        controller: controller,
        maxLines: 1,
        decoration: InputDecoration.collapsed(hintText: fieldHint),
        onEditingComplete: () {
          _pushFieldToDB(key , controller.text);
          // dismiss keyboard
          FocusScope.of(context).requestFocus(new FocusNode());
        },
      ),
    );
  }
  Widget _profileTop() {
    return Container(
        color: Colors.tealAccent,
        height: 250,
        child: Stack(children: <Widget>[
          _profileImageAndBackground(),
          _profileImageIcons(),
        ])); //TODO: get user image or default
  }

  Widget _profileImageAndBackground() {
    return Center(
      child: Image.asset(
        "assets/images/weddingsLogoNoName.png",
        height: 200,
      ),
    );
  }

  Widget _profileImageIcons() {
    return Positioned(
      bottom: 10,
      right: 10,
      child: IconButton(
        icon: Icon(
          Icons.edit,
          color: Colors.grey,
        ),
        onPressed: () {
          //TODO: update profile Pic
          //TODO: and/or Background Pic
        },
      ),
    );
  }

//
//  void _getCurrentUser() async {
//    try {
//      _user = FirebaseAuth.instance.currentUser();
//    } catch (e) {
//      print(e.toString());
//    }
//  }

  void _setUserFields() async {
    try {
      // set fields
      _user = await FirebaseAuth.instance.currentUser();
      DocumentSnapshot snapshot = await Firestore.instance.collection('/users3')
          .document(_user.uid).get();
      _firstName.text = await snapshot.data['firstname'];
      _lastName.text = await snapshot.data['lastname'];
    } catch (e) {
      print(e.toString());
    }
  }

  void _pushFieldToDB(String key, String value) async {
    await Firestore.instance.collection("/users3").document(_user.uid).updateData({key : value});
  }
}

// Used to clear focus thanks, https://stackoverflow.com/questions/44991968/how-can-i-dismiss-the-on-screen-keyboard/44991969#44991969
//FocusScope.of(context).requestFocus(new FocusNode());