
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:core';
// import 'dart:io';

List<String> places = [
  'city_1.jpg',
  'city_2.jpg',
  'forest_1.jpg',
  'forest_2.jpg',
  'lake_1.jpg',
  'lake_2.jpg',
  'ocean_1.jpg',
  'ocean_2.jpg',
  'ocean_3.jpg',
  'planes_1.jpg',
];


class VenuesPage extends StatelessWidget {


  List<Card> _buildVenues(BuildContext context){

    List<String> _places = places;
    return _places.map( (element)=>
      Card(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            AspectRatio(
              aspectRatio: 1.6/1,  //TODO: adjust later
              child: Image.asset(
                'assets/images/places/' + element,
                fit: BoxFit.fitWidth,
              ),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(element,),
                    SizedBox(height: 8.0,),
                    Text('Description'),
                  ],
                ),
              ),
            ),
          ],
        ),
        shape: BeveledRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(5.0))
        ),
      )
    ).toList();
  }
 /* GridView.count(
  crossAxisCount: 2,
  padding: EdgeInsets.all(16.0),
  childAspectRatio:  1.0,
  children: _buildVenues(context),
  ),*/

  TextEditingController _searchValue = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Venues'),
        leading: IconButton(
          icon: Icon(Icons.menu),
          onPressed: () {
            print('MenuButton');
            _buildVenues(context);
          }
        ),
      ),
      body: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
            child: TextField(
              controller: _searchValue,
              decoration: InputDecoration(
                labelText: 'Search',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
              ),
            ),
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              padding: EdgeInsets.all(16.0),
              childAspectRatio:  1.0,
              children: _buildVenues(context),
            ),
          ),
        ],
      ),

      bottomNavigationBar: ButtonBar(
        alignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              print('Search button');
            },
          ),
          IconButton(
            icon: Icon(Icons.chat),
            onPressed: () {
              print('Chat button');
            },
          ),
          IconButton(
            icon: Icon(Icons.account_box),
            color: Color(0xFFAA5585),
            onPressed: () {
              print('Account button');
            },
          ),
          IconButton(
            icon: Icon(Icons.mail),
            color: Color(0xFF913059),
            tooltip: "Inbox",
            onPressed: () {
              print('message button');
            },
          ),
          IconButton(
            icon: Icon(Icons.favorite_border),
            onPressed: () {
              print('Favorites button');
            },
          ),
        ],
      ),
    );
  }
}