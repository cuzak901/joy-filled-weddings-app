import 'package:flutter/material.dart';
import 'homePage.dart';
import 'loginPage.dart';
import 'venuesPage.dart';
import 'accountPage.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Joyfilled Weddings',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.pink,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(
          title: 'Joy Filled Weddings',
        ),
        '/loginPage': (context) => LoginPage(),
        '/venuesPage': (context) => VenuesPage(),
        '/accountPage': (context) => AccountPage(),
      },
      //home: MyHomePage(title: 'Joyfilled Weddings'),
      //initialRoute: "/login",
      //onGenerateRoute: _getRoute,
    );
  }

  /*Route<dynamic> _getRoute(RouteSettings settings) {
    if (settings.name != '/login') {
      return null;
    }

    return MaterialPageRoute<void>(
      settings: settings,
      builder: (BuildContext context) => LoginPage(),
      fullscreenDialog: true,
    );
  }*/
}